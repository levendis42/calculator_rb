require 'operator'

class Parser
  def tokenize(expr)
    token_chars = expr.split(/\s*/)
    token_chars.map do |char|
      is_number?(char) ? char.to_f : Operator.new(char)
    end
  end

  def is_number?(str)
    str.to_f.to_s == str || str.to_i.to_s == str
  end
end
