class Operator
  VALID_OPERATORS = ['+', '-', '*', '/', '%']

  attr_accessor :token

  def initialize(token)
    if VALID_OPERATORS.include? token
      self.token = token
    else
      raise UnsupportedOperator
    end
  end

  def priority
    case self.token
    when '/' then 1
    when '*' then 2
    when '%' then 2
    when '+' then 3
    when '-' then 3
    end
  end

  def perform(operand1, operand2)
    case self.token
    when '+' then operand1.to_f + operand2.to_f
    when '-' then operand1.to_f - operand2.to_f
    when '*' then operand1.to_f * operand2.to_f
    when '/' then operand1.to_f / operand2.to_f
    when '%' then operand1.to_f % operand2.to_f
    else raise UnsupportedOperator
    end
  end

end
