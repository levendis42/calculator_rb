require 'parser'
require 'operator'
require 'exceptions'

class Calculator
  def initialize
    @parser = Parser.new
  end

  def calculate(expr)
    tokens = @parser.tokenize(expr)

    # TODO: validate correctness of expression (starts with
    #       number followed by alternating numbers and operators)


    1.upto(3).each do |priority|
      next_operator_idx = :init
      until next_operator_idx.nil?
        next_operator_idx = tokens.index{|t| t.respond_to?(:priority) && t.priority == priority}
        unless next_operator_idx.nil?
          operand_1_idx = next_operator_idx - 1
          operand_2_idx = next_operator_idx + 1
          result = perform_operation(tokens[next_operator_idx], tokens[operand_1_idx], tokens[operand_2_idx])
          tokens[operand_1_idx..operand_2_idx] = result
        end
      end
    end

    #   TODO: replace subexpression with results
    #   TODO: continue until reduced to 1 number
    #   TODO: return number
    #
    tokens.first
  end

  def perform_operation(operator, operand1, operand2)
    operator.perform operand1, operand2
  end

end
