require File.join(File.dirname(__FILE__), '..', 'spec_helper')

describe Parser do
  let(:parser) { Parser.new }

  describe '#tokenize' do
    it 'correctly tokenizes 1 + 1' do
      tokens = parser.tokenize('1 + 1')
      expect(tokens.first).to eq(1.0)
      expect(tokens.last).to  eq(1.0)
      expect(tokens[1].token).to eq('+')
    end
  end

  describe '#is_number?' do
    it 'recognizes float as number' do
      expect(parser.is_number?('1.29384')).to be(true)
    end

    it 'recognizes integer as number' do
      expect(parser.is_number?('4')).to be(true)
    end

    it 'recognizes operator as not a number' do
      expect(parser.is_number?('+')).to be(false)
    end
  end

end
