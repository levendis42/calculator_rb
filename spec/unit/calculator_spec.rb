require File.join(File.dirname(__FILE__), '..', 'spec_helper')

describe Calculator do
  let(:calculator) { Calculator.new }

  describe '#perform_operation' do
    it 'performs simple addition' do
      expect(calculator.perform_operation(Operator.new('+'), '1', '1')).to eq(2)
    end

    it 'performs simple subtraction' do
      expect(calculator.perform_operation(Operator.new('-'), '2', '1')).to eq(1)
    end

    it 'performs simple multiplication' do
      expect(calculator.perform_operation(Operator.new('*'), '3', '4')).to eq(12)
    end

    it 'performs simple division' do
      expect(calculator.perform_operation(Operator.new('/'), '12', '4')).to eq(3)
    end

    it 'performs simple modulus' do
      expect(calculator.perform_operation(Operator.new('%'), '6', '5')).to eq(1)
    end

    it 'handles division with remainder' do
      expect(calculator.perform_operation(Operator.new('/'), '12', '5')).to eq(2.4)
    end

    it 'orders operands correctly' do
      expect(calculator.perform_operation(Operator.new('-'), '1', '2')).to eq(-1)
    end

    it 'throws an error if unsupported operator is used' do
      expect{calculator.perform_operation(Operator.new('^'), '1', '1')}.to raise_error('UnsupportedOperator')
    end
  end

end
