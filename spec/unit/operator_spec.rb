require File.join(File.dirname(__FILE__), '..', 'spec_helper')

describe Operator do
  it 'can only be initialized with supported operator' do
    expect{ Operator.new('^') }.to raise_error(UnsupportedOperator)
  end

  it 'has a priority based on specified token' do
    expect(Operator.new('/').priority).to eq(1)
    expect(Operator.new('*').priority).to eq(2)
    expect(Operator.new('%').priority).to eq(2)
    expect(Operator.new('+').priority).to eq(3)
    expect(Operator.new('-').priority).to eq(3)
  end

end
