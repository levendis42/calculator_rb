require File.join(File.dirname(__FILE__), '..', 'spec_helper')

describe 'performing calculations' do
  let(:calculator) { Calculator.new }
  it 'can calculate a simple binary operation' do
    expect(calculator.calculate('1 + 1')).to eq(2)
  end

  it 'can combine multiple operations' do
    expect(calculator.calculate('1 + 2 * 3')).to eq(7)
  end

end
